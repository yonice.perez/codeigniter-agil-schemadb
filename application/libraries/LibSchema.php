<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
			
class LibSchema
{

	protected $_ci;

	private $_error  = '';
	
	private $_access = FALSE;
	
	private $_success = array();

	/**
	 * required this element  data['controller'] => $this in constructor
	 */
	
	public function __construct( $data = array() ){

		$this->_ci    = $data['controller'];

		$this->_ci->load->dbforge();		
		$this->_ci->load->config('schema');
		$this->_ci->load->library('Spyc');
		$this->_ci->db->db_debug = FALSE;
				
		$schema_table = config_item('schema_table');

		if($schema_table == ''){
			$this->_error = 'Schema table not asigned';
		}else if ( ! $this->_ci->db->table_exists($schema_table)){	
			$this->_ci->dbforge->add_field(array(				
				'name'    => array('type' => 'varchar', 'constraint' => 100),
				'user'    => array('type' => 'varchar', 'constraint' => 100),
				'last_modify' => array('type' => 'int', 'constraint' => 11),
				'date'    => array('type' => 'datetime', 'notnull' => FALSE, 'default' => '0000-00-00'),
			));	
			$this->_ci->dbforge->create_table( $schema_table );
		}
	}		

	/**
	 * Run version
	 * 
	 * @return complete 
	 */
	public function runMigration( $name  ){
		
		$schemas = $this->getSchemas();
		if(!isset($schemas[$name])){
			$this->_error = 'File not found, check folder schemas';
			return false;
		} else{		
			return $this->_run_file( $schemas[$name], $name);
		}

	}

	/**
	 * Path schema
	 * 
	 * @return complete 
	 */		
	public function getPathSchema(){
		return config_item('schema_path');
	}	


	/**
     * Login
     *
     * I can acces to config schema
     * 
     * @return boolean
     */
	public function login( $usu, $pass){
		
		$sessionVar = config_item('schema_session_var') ? config_item('schema_session_var') : 'session_schema';
		$dataUsers  = config_item('schema_session_users');
	
		if( isset( $dataUsers[$usu] ) ) {
		
			if($dataUsers[$usu] === $pass) {	
			
				$data[$sessionVar]	    = $usu;
 				$this->_ci->session->set_userdata($data);
				return TRUE;
			}
		}
		return FALSE;
	}

	/**
     * Logout
     *
     * Close session var
     * 
     * @return void
     */	
	public function logout(){
		$sessionVar   	   = config_item('schema_session_var') ? config_item('schema_session_var') : 'session_schema';
		if($sessionVar!=''){
			$data[$sessionVar] = '';
			$this->_ci->session->set_userdata($data);	
		}
	}

	/**
     * Exist login
     *
     * Get user if exist login
     * 
     * @return string (user)
     */
	public function isLogged(){
		$sessionVar = config_item('schema_session_var') ? config_item('schema_session_var') : 'session_schema';
		$dataUsers  = config_item('schema_session_users');

		if(isset($this->_ci->session->userdata[$sessionVar])){

			$usu = $this->_ci->session->userdata[$sessionVar];
			
			if( isset( $dataUsers[$usu] ) ){
				return $usu;
			}
		}
		return FALSE;
	}

	/**
	 * Get schemas db
	 *
	 *  Mi list tables migrated
	 *
	 * @return string
	 */	
	public function getSchemasMigrated(){
		$this->_ci->db->order_by('date','desc');
				
		return $this->_ci->db
			->get( config_item('schema_table') )
			->result();
	}	

	/**
     * get Pending schemass
     *
     * List pending schemas use in the view schema
     * 
     * @return array [version][namefile]
     */	
	public function getSchemasPending(){
		$list_schemas 	 		= $this->getSchemas();
		$list_scehmas_migrated 	= $this->getSchemasMigrated();
		$pending_schemas = array();

		foreach ($list_schemas as $name => $file) {
			if(!$this->_get_schema_exec($name)){
				$pending_schemas[$name] = $file;
			}
		}	
		return $pending_schemas;
	}

	/**
     * Find schema database
     *
     * Get all list off schema
     * 
     * @return array [version][namefile]
     */	
	public function getSchemas(){
		$schemas = array();

		// Load all *_*.yml files in the migrations path
		foreach (glob(config_item('schema_path').'*.yml') as $file)
		{
			$name = basename($file, '.yml');
			$schemas[$name] = $file; 	
		}		

		ksort($schemas);
		return $schemas;
	}

	/**
     * Find schema database
     *
     * Get all list off schema
     * 
     * @return array [version][namefile]
     */	
	public function getSchemasLastModify(){
		$schemas = array();
		
		// Load all *_*.yml files in the migrations path
		foreach (glob(config_item('schema_path').'*.yml') as $file)
		{	
			$name           = basename($file, '.yml');
			$schemas[$name] = filemtime($file); 	
		}		

		ksort($schemas);
		return $schemas;
	}


	/**
	 * Get message erros
	 *
	 *  exist message errors?
	 *
	 * @return string
	 */
	public function getError(){

		return $this->_error;
	}

	/**
	 * Get message success
	 *
	 * All message correct
	 *
	 * @return string
	 */
	public function getSuccess(){
		$html = '';
		foreach ($this->_success as $value) {
			$html.= "<p>".$value."</p>";
		}
		return $html;
	}
	
	private function _run_file( $file , $nameSchema ){
			
		$tables 	    = $this->_ci->spyc->YAMLLoad( $file );
		
		$list_tables_bd = $this->_ci->db->list_tables();
			
		foreach ($tables as $table =>  $fields) {
			
			$_droptable = (isset($fields['_droptable']) ) ? $fields['_droptable'] : false;

			if($_droptable){	

				$this->_ci->dbforge->drop_table($table);
				if($this->_error = $this->_error_db()){
					$this->_success[] = 'dbforge drop_table '.$table.' not found';
					continue;
					//return false;	
				}else{
					$this->_success[] = 'dbforge drop_table '.$table.' correct';
					continue;
				}
			}		
			
			

			if(!in_array( $table, $list_tables_bd)){
				$new_table 	  = true;
				$list_fields  = Array();
			}else{		
				$new_table 	  = false;
				$list_fields  = $this->_ci->db->list_fields( $table ); 
			}
				
			$modify_column  = $add_column = $add_field =  $primary_key = null;

			foreach ($fields as $name_field => $attr) {

				$primary      = isset( $attr['primary'] ) ? $attr['primary'] : false;
				$_createindex = isset( $attr['_createindex'] ) ? $attr['_createindex'] : false;
				$_dropcolumn  = isset( $attr['_dropcolumn'] ) ? $attr['_dropcolumn'] : false; 
				
				if($_dropcolumn){
					$this->_ci->dbforge->drop_column($table, $name_field);	
					if($this->_error = $this->_error_db()){
						return false;	
					}else{
						$this->_success[] = 'dbforge drop_column '.$name_field.' correct';
						continue;
					}
				}

				if($new_table){
					$add_field[$name_field] = $attr;
				}else if(in_array($name_field, $list_fields)){
					$modify_column[$name_field] = $attr;	
				} else{			
					$add_column[$name_field] 	= $attr;
				}	
									
				
				if( $primary){			
					if(!$new_table){
						$primary_key[] = $name_field;
					}else{	
						$this->_ci->dbforge->add_key( $name_field, TRUE );
					}
						
				}	
				if( $_createindex ){
					$this->_ci->dbforge->add_key( $name_field, FALSE );
				}	
			}

			if($new_table)
			{	

				if(!count($this->_ci->dbforge->primary_keys)){		
					$this->_error = 'Must primary key is required while create table';
					return false;		
				}
				$this->_ci->dbforge->add_field($add_field);
				$this->_ci->dbforge->create_table( $table );

				if($this->_error = $this->_error_db()){
					return false;	
				}else{
					$this->_success[] = 'dbforge create_table '.$table.' correct';
				}
			} else
			{	
				if(count($primary_key)){
					if(!$this->_add_primary_key($table, $primary_key)){
						return false;	
					}else{
						$this->_success[] = 'transact-sql primary key was added '.$table;
					}
				}
				if(count($modify_column)){
					$this->_ci->dbforge->modify_column( $table, $modify_column );
					if($this->_error = $this->_error_db()){
						return false;	
					}else{
						$this->_success[] = 'dbforge modify_column '.$table.' correct';
					}
				}
				if(count($add_column)){
					$this->_ci->dbforge->add_column( $table, $add_column  );
					if($this->_error = $this->_error_db()){
						return false;	
					}else{
						$this->_success[] = 'dbforge add_column '.$table.' correct';
					}
				}
			}
		}

		$schema_log['user']           = $this->isLogged();
		$schema_log['date']           = date('Y-m-d H:i:s');
		$schema_log['name']           = $nameSchema; 
		$schema_log['last_modify'] = filemtime($file); 

		if($this->_get_schema_exec($nameSchema)){	
			$this->_ci->db->update(config_item('schema_table'), $schema_log, array('name' => $nameSchema) );
		}else{
			$this->_ci->db->insert(config_item('schema_table'),$schema_log);
		}	
		
		return $schema_log;	
	}
	
	private function _get_schema_exec( $name ){
		return $this->_ci->db
					->get_where( config_item('schema_table') , 
							[ 'name' => trim($name) ] )->row();
	}	

	private function _error_db(){
		$msg_error = $this->_ci->db->error();
		if($msg_error['message']!=''){
			return $msg_error['message'];
		}	
		return false;
	}

	private function _add_primary_key( $table, $keys){
			
		$sql 	= " ALTER TABLE ".$table." DROP PRIMARY KEY, ADD PRIMARY KEY(".implode(",",$keys).") ";
		$result = $this->_ci->db->query($sql);	
		if($this->_error = $this->_error_db()){
			return false;
		}	
		return true;
	}	

}	