<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @description:
 */
class Schema extends CI_Controller
{		
	function __construct()
	{		

		parent::__construct();
		
		$this->load->helper('url');	
		$this->load->library('session');
		$this->load->database();
		$this->load->library( 'LibSchema', ['controller' => $this ]);
			
	}


	public function index(){
				
		if( $var['user'] = $this->libschema->isLogged()){
			$this->dashboard();
		}else{
			$this->load->view('schema/view-login-schema');
		}
		
	}	

	public function login(){
		
		$can_login = $this->libschema->login( 
						$this->input->post('user'),  
						$this->input->post('password')
					);

		if ( $can_login  ){
			redirect('/private/schema/dashboard/');
		}else{
			$var['content'] = 'User or password not valid';	
			$this->load->view('schema/view-login-schema' , $var);
		}
	}	

	public function dashboard(){	
			
		if( $var['user'] = $this->libschema->isLogged()){	
			$var['list_schemas']          = $this->libschema->getSchemasPending(); 
			$var['list_schemas_migrated'] = $this->libschema->getSchemasMigrated(); 
			$var['last_modify']           = $this->libschema->getSchemasLastModify();
			//die('<pre>'.print_r($var,1).'</pre>');
			$this->load->view('schema/view-dashboard-schema', $var);		
		}else{
			redirect('/private/schema/');
		}
	}

	public function runmigration(){
		if($this->libschema->isLogged()){
			if ( $response = $this->libschema->runMigration( $this->input->post('name')  )){
				echo json_encode( [	'status' => 1, 
									'schema_log' => $response,
									'message_success' => $this->libschema->getSuccess() ]);	
			}else{	
				echo json_encode( ['status' => 0, 
						'msg' => $this->libschema->getError(),
						'message_success' =>  $this->libschema->getSuccess()
					]);		
			}
			
		}
	}	

	public function logout(){	
		$this->libschema->logout();
		redirect('/private/schema/');
	}

}

/* End of file 'Schema' */
/* Location: ./application/controllers/Schema.php */