<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Enable/Disable Schema
|--------------------------------------------------------------------------
|
| Schema are disabled by default for security reasons.
| You should enable migrations whenever you intend to do a schema migration
| and disable it back when you're done.
|
*/
$config['schema_enable'] = TRUE;
	

/*
|--------------------------------------------------------------------------
| Schema table
|--------------------------------------------------------------------------
|
| this table save version, name file, date and user. Use for run pending 
| schema yml
|
*/
$config['schema_table'] = 'schema_ci';	

/*
|--------------------------------------------------------------------------
| Schema session var
|--------------------------------------------------------------------------
|
| name session to login
|	
*/
$config['schema_session_var'] = 'session_schema';

/*
|--------------------------------------------------------------------------
| Schema users controller
|--------------------------------------------------------------------------
|
| you have acces to use enviroment visual to execute migration
|	
*/	
$config['schema_session_users'] = Array(
	'admin' => 'secret'
);

/*
|--------------------------------------------------------------------------
| schemas Path
|--------------------------------------------------------------------------
|
| Path to your migrations folder.
| Typically, it will be within your application path.
| Also, writing permission is required within the migrations path.
|
*/
$config['schema_path'] = APPPATH.'schema/';
