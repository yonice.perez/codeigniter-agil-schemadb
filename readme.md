# codeigniter agil schemadb #

## Introduction:
Too human to be true

## **Advantage**
:heavy_check_mark: Admin users from config schema.

:heavy_check_mark: log migration

:heavy_check_mark: friendly interface (bootstrap theme).

:heavy_check_mark: catch errors db.

## Requirements

* codeigniter 3.0 +
* Download files library, controller, views and config
* Create folder schema in APPPATH (This is where our structure will be migration, remember create htacces [Deny from all])

## How does it work
yml is very easy, create file yml (my_tables_example.yml) on folder application/schema/ or define your schema_path in config, follow this format
```yml
tablename1:
    namecolumn:
        type: integer
        default: 0
tablename2:
    namecolumn:
        type: varchar
        default: ''
        constraint: 150
```

#### Attr extra ####
create index i_nametable_name_column on table( namecolumn );
```yml
tablename2:
    namecolumn:
        type: integer
        default: 0
        _createindex: tablename1.namecolumn
```
alter table table drop column name_column
```yml
tablename2:
    namecolumn:
       _dropcolumn: true

```
drop table table
```yml
tablename2:
    _droptable: true
```

### run  schema pending ###
acces to url: mysiteurl/**private/schema**

add user default: **admin** password: **adminsecret** (edit or add on config/schema.php)

![alt text](img.png "Migration schema")